/*
 * Copyright 2013 Blue Lotus Software, LLC..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluelotussoftware.example.jsf.ajax;

import java.util.UUID;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionListener;
import javax.inject.Named;

/**
 * Page backing bean for
 * <code>index.xhtml</code>.
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@Named
@RequestScoped
public class IndexBean {

    private String uuid;

    /**
     * {@link ActionListener} implementation that generates a random UUID and
     * sets the value in the bean. The returned value can be found by calling
     * {@link #getUuid()}.
     */
    public void generateUUID() {
        try {
            // Add this so that we have a delay for the AJAX spinner.
            Thread.sleep(2000);
            uuid = UUID.randomUUID().toString();
        } catch (InterruptedException ignore) {
        }
    }

    /**
     * Getter for UUID.
     *
     * @return return {@link UUID#toString()}.
     */
    public String getUuid() {
        return uuid;
    }

}
